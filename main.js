function isUrl(s) {
	var regexp =
		/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	return regexp.test(s);
}

const cleanText = (s) => s.trim().replace(/\s\s+/g, " ");

const parseHtml = async (amazonUrl, productId) => {
	// Get product HTML page
	const response = await fetch(amazonUrl);

	// Prepare HTML parser
	const rewriter = new HTMLRewriter();
	const matches = {};
	const selectors = [
		{ name: "title", val: "#title" },
		{ name: "availability1", val: "#availability" },
		{
			name: "availability2",
			val: "#exports_desktop_outOfStock_buybox_message_feature_div > div:nth-child(1) > span:nth-child(1)",
		},,
		{
			name: "availability3",
			val: "#partialStateBuybox > div:nth-child(1) > div:nth-child(1) > span:nth-child(1)",
		},
	];

	try {
		selectors.forEach((selector) => {
			matches[selector.name] = [];

			let nextText = "";

			rewriter.on(selector.val, {
				element(element) {
					matches[selector.name].push(true);
					nextText = "";
				},

				text(text) {
					nextText += text.text;

					if (text.lastInTextNode) {
						matches[selector.name].push(nextText);
						nextText = "";
					}
				},
			});
		});
	} catch (error) {
		console.log(error);
		return;
	}

	const transformed = rewriter.transform(response);
	await transformed.text();

	selectors.forEach((selector) => {
		const nodeCompleteTexts = [];
		let nextText = "";

		matches[selector.name].forEach((text) => {
			if (text === true) {
				if (nextText.trim() !== "") {
					nodeCompleteTexts.push(cleanText(nextText));
					nextText = "";
				}
			} else {
				nextText += text;
			}
		});

		const lastText = cleanText(nextText);
		if (lastText !== "") nodeCompleteTexts.push(lastText);
		matches[selector.name] = nodeCompleteTexts;
	});

	return matches;
};

export default {
	async fetch(request, env) {
		if (request.method === "POST") {
			const payload = await request.json();
			console.log(payload);

			// Checking if the payload comes from Telegram
			if (!payload?.message?.message_id) return new Response("OK");
			if (!payload?.message?.chat?.id) return new Response("OK");
			if (!payload?.message?.text) return new Response("OK");
			const {
				message_id,
				chat: { id: chatId },
				text,
			} = payload.message;

			// Check for valid amazon URL
			if (!text || !isUrl(text) || text.indexOf("https://www.amazon.com") < 0)
				return new Response("OK");

			let amazonUrls = text.split("\n");

			// Ensure URL is unique
			amazonUrls = [...new Set(amazonUrls)];

			for (const amazonUrl of amazonUrls) {
				// Get product ID
				let productId = amazonUrl.match(/\/dp\/.{10}/gs);
				if (!productId?.length) continue;
				if (productId?.length) productId = productId[0].replace("/dp/", "");

				// Get availability and title
				const result = await parseHtml(amazonUrl, productId);
				if (!result) continue;
				console.log(result);

				// Clean up result
				let availability = "Unknown";
				if (result.availability1.length) {
					availability = result.availability1[0];
				} else if (result.availability2.length) {
					availability = result.availability2[0];
				} else if (result.availability3.length) {
					availability = `${result.availability3[0]} - Available?`;
				}

				let toSend = `Product: ${encodeURIComponent(result.title)}\n\nStock: ${encodeURIComponent(availability)}`;

				// Prepare telegram URL
				const telegramUrl = `https://api.telegram.org/bot${env.TELEGRAM_API_KEY}/sendMessage?chat_id=${chatId}&text=${toSend}&reply_to_message_id=${message_id}`;

				// Calling the API endpoint to send a telegram message
				await fetch(telegramUrl).then((resp) => resp.json());
			}
		}

		return new Response("OK");
	},
};
