<!-- PROJECT SHIELDS -->

[![Starrers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h3 align="center">Amazon Got Stock?</h3>
</div>

# About The Project

This is a inventory stock checker for Amazon products, powered by Cloudflare Worker and Telegram Bot API.

## Built With

[![JavaScript][js]][js-url]
[![Cloudflare][cloudflare]][cloudflare-url]
[![Telegram][telegram]][telegram-url]

# Getting Started

To get a copy up and running follow these simple steps.

## Prerequisites

1. Create a new [Telegram Bot](https://core.telegram.org/bots/features#botfather)

1. Set up account for [Cloudflare Worker](https://workers.cloudflare.com/)

1. Create a worker in Cloudflare

1. Go to `Settings > Variables`

1. Create a variable under `Environment Variables` with the following details

   1. Variable name: `TELEGRAM_API_KEY`
   1. Value: `YOUR_TELEGRAM_BOT_TOKEN`

1. Add the codes in [main.js](main.js) to the `Quick edit` page

## Usage

1. Send a (or multiple) links to the telegram bot

1. The bot will reply with the following:

   1. In stock

      ```
      Product: PRODUCT_NAME

      Stock: In Stock
      ```

   1. X stock remaining

      ```
      Product: PRODUCT_NAME

      Stock: Only X left in stock - order soon
      ```

   1. Out of stock

      ```
      Product: PRODUCT_NAME

      Stock: Currently unavailable. We don't know when or if this item will be back in stock.
      ```

   1. Products with no obvious stock, but assumed to have stock

      ```
      Product: PRODUCT_NAME

      Stock: To buy, select Size - Available?
      ```

   1. Unknown stock

      ```
      Product: PRODUCT_NAME

      Stock: Unknown
      ```

<!-- CONTRIBUTING -->

# Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->

# License

Distributed under the GNU General Public License v3.0. See `LICENSE` for more information.

# Special Thanks

Cloudflare's [Web Scraper](https://workers.cloudflare.com/built-with/projects/web-scraper)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[stars-shield]: https://img.shields.io/gitlab/stars/nootish/amazon-got-stock.svg?style=for-the-badge
[stars-url]: https://gitlab.com/nootish/amazon-got-stock/-/starrers
[issues-shield]: https://img.shields.io/gitlab/issues/open/nootish/amazon-got-stock.svg?style=for-the-badge
[issues-url]: https://gitlab.com/nootish/amazon-got-stock/issues
[license-shield]: https://img.shields.io/gitlab/license/nootish/amazon-got-stock.svg?style=for-the-badge
[license-url]: https://gitlab.com/nootish/amazon-got-stock/-/blob/master/LICENSE
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/yichou/
[js]: https://img.shields.io/badge/JavaScript-000000?style=for-the-badge&logo=JavaScript&logoColor=white
[js-url]: https://www.ecma-international.org/publications-and-standards/standards/ecma-262/
[cloudflare]: https://img.shields.io/badge/Cloudflare-000000?style=for-the-badge&logo=Cloudflare&logoColor=white
[cloudflare-url]: https://workers.cloudflare.com/
[telegram]: https://img.shields.io/badge/Telegram-000000?style=for-the-badge&logo=Telegram&logoColor=white
[telegram-url]: https://core.telegram.org/bots/api
